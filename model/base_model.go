package model

import "time"

type BaseModel struct {
	ID        string     `json:"id" bson:"id,omitempty"`
	CreatedAt *time.Time `json:"createdAt" bson:"createdAt,omitempty"`
	UpdatedAt *time.Time `json:"updatedAt" bson:"updatedAt,omitempty"`
}
