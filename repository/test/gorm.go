package main

import (
	"context"
	"gitlab.com/phongthien/repository/repository"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
)

var dsn = ""

type Product struct {
	gorm.Model
	Code  string
	Price uint
}

func main() {

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("failed to connect database")
	}
	repo := repository.BaseGormRepository[Product]{
		Db:    db.Unscoped(),
		Table: "products",
	}

	//count, err := repo.Count(context.TODO(), repository.BuildQuery("1=1"))
	//log.Print(count)
	//result, err := repo.FindOne(context.TODO(), repository.BuildQuery("code=?", "F56"))
	//log.Print(result, err)
	//result, err = repo.UpdateOne(context.TODO(), repository.BuildQuery("code=?", "F42"), Product{
	//	Model: gorm.Model{},
	//	Code:  "F56",
	//	Price: 0,
	//})
	//result, err := repo.Upsert(context.TODO(), repository.BuildQuery("code=?", "F64"), &Product{
	//	Model: gorm.Model{},
	//	Code:  "F642",
	//	Price: 0,
	//})
	//log.Print(result, err)
	//result, err := repo.Upsert(context.TODO(), repository.BuildQuery("code=?", "F64"), &Product{
	//	Model: gorm.Model{},
	//	Code:  "F642",
	//	Price: 0,
	//})
	//result, err := repo.CreateOne(context.TODO(), &Product{
	//	Model: gorm.Model{},
	//	Code:  "create",
	//	Price: 0,
	//})
	//log.Print(result, err)
	results, err := repo.FindAll(context.TODO(),repository.BuildQuery("code=?", "F64"), nil,nil)
	log.Print(results, err)

}
