package main

//
//import (
//	"context"
//	"go-example/common/repository"
//	"go.mongodb.org/mongo-driver/bson"
//	"go.mongodb.org/mongo-driver/mongo"
//	"log"
//)
//
//type TestModel struct {
//	ID   string `json:"id" bson:"_id,omitempty"`
//	Name string `json:"name" bson:"name"`
//}
//type ITestRepository interface {
//	repository.IRepository[TestModel]
//}
//type TestRepository struct {
//	repository.IRepository[TestModel]
//}
//
//func NewTestRepository(db *mongo.Database, collectionName string) (ITestRepository, error) {
//	return &TestRepository{
//		repository.NewBaseRepository[TestModel](db, collectionName),
//	}, nil
//
//}
//
//func main() {
//	client, _ := repository.BasicConnection(context.TODO(), "mongodb://localhost:27017/my_database", "my_database")
//	aRepository, _ := NewTestRepository(client, "test")
//	a := &TestModel{
//		Name: "test",
//	}
//	aRepository.CreateOne(context.TODO(), a)
//	aRepository.Upsert(context.TODO(), bson.M{"name": "test2"}, a)
//	aRepository.UpdateOne(context.TODO(), bson.M{"name": "test2"}, bson.M{"name": "no nests"})
//	g, err := aRepository.FindOne(context.TODO(), bson.M{"name": "no nests"})
//	log.Print(g, err)
//	count, err := aRepository.Count(context.TODO(), bson.M{"name": "no nests"})
//	log.Print(count, err)
//	as, err := aRepository.FindAll(context.TODO(), bson.M{"name": "no nests"}, &repository.Paginate{
//		Offset: 0,
//		Limit:  2,
//	}, nil)
//	log.Print(as, err)
//	as, err = aRepository.FindAll(context.TODO(), bson.M{}, &repository.Paginate{
//		Offset: 0,
//		Limit:  2,
//	}, (*repository.Sort)(&map[string]string{
//		"name": "asc",
//	}))
//	log.Print(as[0], err)
//	//log.Print(upsertRecord)
//
//}
